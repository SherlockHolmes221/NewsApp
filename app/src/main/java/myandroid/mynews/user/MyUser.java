package myandroid.mynews.user;

//用户信息类，存放用户的手机号码，密码信息
public class MyUser{


	private String telephone;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "MyUser{" +
				"telephone='" + telephone + '\'' +
				", password='" + password + '\'' +
				'}';
	}
}
