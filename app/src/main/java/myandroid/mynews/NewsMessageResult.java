package myandroid.mynews;

import android.graphics.Bitmap;

/**
 * Created by quxia on 2017/7/21.
 */
public class NewsMessageResult {
    private int code;//返回码 302000
    private String message;//返回的信息"亲，已帮您找到相关新闻" 或者 发送的信息"我想看新闻"

    private String newsArticle1;//返回的新闻标题
    private String newsSource1;//返回的新闻来源，网易新闻
    private Bitmap icon1;//返回的新闻图片
    private String url1;//返回的新闻链接


    private String newsArticle2;
    private String newsSource2;
    private Bitmap icon2;
    private String url2;

    private String newsArticle3;
    private String newsSource3;
    private Bitmap icon3;
    private String url3;

    public NewsMessageResult() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNewsArticle1() {
        return newsArticle1;
    }

    public void setNewsArticle1(String newsArticle1) {
        this.newsArticle1 = newsArticle1;
    }

    public String getNewsSource1() {
        return newsSource1;
    }

    public void setNewsSource1(String newsSource1) {
        this.newsSource1 = newsSource1;
    }

    public Bitmap getIcon1() {
        return icon1;
    }

    public void setIcon1(Bitmap icon1) {
        this.icon1 = icon1;
    }

    public String getUrl1() {
        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }

    public String getNewsArticle2() {
        return newsArticle2;
    }

    public void setNewsArticle2(String newsArticle2) {
        this.newsArticle2 = newsArticle2;
    }

    public String getNewsSource2() {
        return newsSource2;
    }

    public void setNewsSource2(String newsSource2) {
        this.newsSource2 = newsSource2;
    }

    public Bitmap getIcon2() {
        return icon2;
    }

    public void setIcon2(Bitmap icon2) {
        this.icon2 = icon2;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getNewsArticle3() {
        return newsArticle3;
    }

    public void setNewsArticle3(String newsArticle3) {
        this.newsArticle3 = newsArticle3;
    }

    public String getNewsSource3() {
        return newsSource3;
    }

    public void setNewsSource3(String newsSource3) {
        this.newsSource3 = newsSource3;
    }

    public Bitmap getIcon3() {
        return icon3;
    }

    public void setIcon3(Bitmap icon3) {
        this.icon3 = icon3;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }
}
