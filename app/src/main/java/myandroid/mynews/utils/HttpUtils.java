package myandroid.mynews.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;

import myandroid.mynews.CodeResult;
import myandroid.mynews.NewsMessage;

//接口为图灵机器人的接口，只用里面的新闻功能
//遇到了很大的问题，这个机器人无法返回编码为302000的新闻类
//还有就是不知道怎么获取图片

public class HttpUtils {

    //在网上申请的接口
    private static final  String URL = "http://www.tuling123.com/openapi/api";
    private static final  String API_KEY = "66f43a43c28f407facae6719d5f6cac4";

    public static NewsMessage sendMessage(String msg, Context context){
        NewsMessage newsMessage = null;
        CodeResult codeResult = null;

        try {
            String jsonRes = doGet(msg);
            Gson gson = new Gson();

            //获取返回码
            codeResult = gson.fromJson(jsonRes,CodeResult.class);
            int code = codeResult.getCode();

            //若返回码为302000，则返回NewsMessage对象，否则返回null
            switch(code){
                case 302000:
                    try {
                        newsMessage = gson.fromJson(jsonRes,NewsMessage.class);
                    }catch (JsonIOException e){
                        e.printStackTrace();
                        Log.e("TAG","服务器繁忙，请稍后再试");
                    }
                    newsMessage.setDate(new Date());
                    newsMessage.setType(NewsMessage.Type.INCOME);
                    break;
                default:
                  //  Toast.makeText(context,"服务器繁忙，请稍后再试", Toast.LENGTH_SHORT).show();
                    Log.e("TAG","返回数据错误！");
                    break;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e("TAG","服务器繁忙，请稍后再试");
        }
        return newsMessage;
    }

    public static String doGet(String msg) throws MalformedURLException {

        String result = "";
        String url = setParams(msg);
        InputStream input = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

       //联网
        try {
            java.net.URL urlNet= new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlNet.openConnection();
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(5000);
            conn.setRequestMethod("GET");

            input = conn.getInputStream();
            int len = -1;
            byte[] buf = new byte[128];
            byteArrayOutputStream = new ByteArrayOutputStream();

            while((len = input.read(buf))!=-1){
                byteArrayOutputStream.write(buf,0,len);
            }

            byteArrayOutputStream.flush();
            result = new String(byteArrayOutputStream.toByteArray());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(byteArrayOutputStream != null){
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(input != null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.e("TAG",result);
        return result;
    }


    //转换成可识别的URL
    private static String setParams(String msg) {
        String url = null;
        try {
            url = URL+ "?key="+API_KEY+"&info="+ URLEncoder.encode(msg,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return url;
    }
}
