package myandroid.mynews.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import myandroid.mynews.NewsMessage;
import myandroid.mynews.R;

/**
 * Created by quxia on 2017/7/20.
 */

//新闻listView的适配器
public class NewsAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private List<NewsMessage> mNewsMessageList;

    //构造函数
    public NewsAdapter(Context context, List<NewsMessage> mNewsMessageList) {
        mLayoutInflater = LayoutInflater.from(context);
        this.mNewsMessageList = mNewsMessageList;
    //    Log.e("TAG","NewsAdapter");
    }

//    //重写 getItemViewType  getViewTypeCount 函数，由于有发送和接受信息两种类型
//    //0——接收的信息 1——发送的信息
//    @Override
//    public int getItemViewType(int position) {
//       NewsMessage newsMessage = mNewsMessageList.get(position);
//        if(newsMessage.getType() == NewsMessage.Type.INCOME){
//            return 0;
//        }
//        return 1;
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }

    @Override
    public int getCount() {
        return mNewsMessageList.size();
    }

    @Override
    public Object getItem(int i) {
        return mNewsMessageList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.e("TAG","viewHolder");

        NewsMessage newsMessage = mNewsMessageList.get(i);
        ViewHolder viewHolder =null;

        //当view==null的时候，初始化 viewHolder
        if(view == null){
            view = mLayoutInflater.inflate(R.layout.news_item,viewGroup,false);
            viewHolder = new ViewHolder();
            viewHolder.mDate  = (TextView) view.findViewById(R.id.id_msg_date);

            viewHolder.newsArticle1 = (TextView)view.findViewById(R.id.id_msg_article1);
            viewHolder.newsArticle2 = (TextView)view.findViewById(R.id.id_msg_article2);
            viewHolder.newsArticle3 = (TextView)view.findViewById(R.id.id_msg_article3);

            viewHolder.newsSource1 = (TextView)view.findViewById(R.id.id_msg_source1);
            viewHolder.newsSource2 = (TextView)view.findViewById(R.id.id_msg_source2);
            viewHolder.newsSource3 = (TextView)view.findViewById(R.id.id_msg_source3);

            viewHolder.url1 = (TextView) view.findViewById(R.id.id_msg_url1);
            viewHolder.url2 = (TextView) view.findViewById(R.id.id_msg_url2);
            viewHolder.url3 = (TextView) view.findViewById(R.id.id_msg_url3);

            viewHolder.icon1 = (ImageView)view.findViewById(R.id.id_msg_icon1);
            viewHolder.icon2 = (ImageView)view.findViewById(R.id.id_msg_icon2);
            viewHolder.icon3 = (ImageView)view.findViewById(R.id.id_msg_icon3);

            view.setTag(viewHolder);

        }
        //当view!=null,获取已经初始化的标签
        else{
            viewHolder = (ViewHolder)view.getTag();
        }
        //设置viewHolder与一个newsMessage对象绑定
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        viewHolder.mDate.setText(simpleDateFormat.format(newsMessage.getDate()));

        viewHolder.newsArticle1.setText(newsMessage.getNewsArticle1());
        viewHolder.newsArticle2.setText(newsMessage.getNewsArticle2());
        viewHolder.newsArticle3.setText(newsMessage.getNewsArticle3());

        viewHolder.newsSource1.setText(newsMessage.getNewsSource1());
        viewHolder.newsSource2.setText(newsMessage.getNewsSource2());
        viewHolder.newsSource3.setText(newsMessage.getNewsSource3());

        viewHolder.icon1.setImageBitmap(newsMessage.getIcon1());
        viewHolder.icon2.setImageBitmap(newsMessage.getIcon2());
        viewHolder.icon3.setImageBitmap(newsMessage.getIcon3());

        viewHolder.url1.setText((CharSequence) newsMessage.getUrl1());
        viewHolder.url2.setText((CharSequence) newsMessage.getUrl2());
        viewHolder.url3.setText((CharSequence) newsMessage.getUrl3());

        return view;
    }

    //定义 ViewHolder
    public final class ViewHolder{

        TextView mDate;

        TextView newsArticle1;
        TextView newsSource1;
        ImageView icon1;
        TextView url1;

        TextView newsArticle2;
        TextView newsSource2;
        ImageView icon2;
        TextView url2;

        TextView newsArticle3;
        TextView newsSource3;
        ImageView icon3;
        TextView url3;
    }
}
