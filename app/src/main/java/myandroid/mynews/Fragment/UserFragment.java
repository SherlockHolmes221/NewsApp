package myandroid.mynews.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import myandroid.mynews.MainActivity;
import myandroid.mynews.R;
import myandroid.mynews.Sqlite.UserDatabaseHelper;
import myandroid.mynews.user.MyUser;

/**
 * Created by quxia on 2017/7/19.
 */

//用户登陆的界面
public class UserFragment extends android.support.v4.app.Fragment {

    private ListView mListview;//登陆界面的listView
    private BaseAdapter mAdapter;//listView的适配器
    private EditText mPhoneNumber, mPassword;//登陆界面的手机号码和密码

    private EditText myPhoneNumber, myPassword;//注册界面的手机号码和密码

    private Button mLoginInButton;

    private UserDatabaseHelper userDatabaseHelper;//用户信息数据库帮助类
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_activity,container,false);
        final Context context = view.getContext();

        mPhoneNumber = (EditText) view.findViewById(R.id.et_number);
        mPassword = (EditText) view.findViewById(R.id.et_password);
        mLoginInButton = (Button)view.findViewById(R.id.bt_login_in);

        //登陆按钮的点击事件
        mLoginInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                String telephone = mPhoneNumber.getText().toString();
                String password = mPassword.getText().toString();

                //如果任何一个为空
                if (telephone.equals("")) {
                    Toast.makeText(context, "手机号码不能为空", Toast.LENGTH_SHORT).show();

                } else if (password.equals("")) {
                    Toast.makeText(context, "密码不能为空", Toast.LENGTH_SHORT).show();

                } else if (!password.equals("") && !telephone.equals("")) {

                    //将获取的用户信息封装到MyUser类中
                    final MyUser user = new MyUser();
                    user.setTelephone(telephone);
                    user.setPassword(password);
                    MyUser myUser = new MyUser();

                    //查找成功的标志
                    boolean flag = false;
                    //定义查找的游标
                    Cursor cursor = userDatabaseHelper.getAllUserData();
                    if(cursor != null){   //开始查找
                        while(cursor.moveToNext()){
                            myUser.setTelephone(cursor.getString(cursor.getColumnIndex("telephone")));
                            myUser.setPassword(cursor.getString(cursor.getColumnIndex("password")));

                            Log.e("TAG",myUser.toString());

                            //信息匹配，登陆成功
                            if(telephone.equals(myUser.getTelephone()) && password.equals(myUser.getPassword())){
                                Toast.makeText(context,"登陆成功",Toast.LENGTH_SHORT).show();
                                flag = true;
                                break;
                            }
                        }
                        cursor.close();
                    }
                    if(!flag){//查找失败
                        Toast.makeText(context,"用户不存在",Toast.LENGTH_SHORT).show();
                    }else{ //查找成功，跳转到新闻主界面
                        Intent intent = new Intent();
                        intent.setClass(context, MainActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        //listView与适配器的绑定并设置点击事件
        mListview = (ListView) view.findViewById(R.id.listview);
        mAdapter = new ArrayAdapter<String>(context, R.layout.login_ist_item,
                R.id.tv_item, getResources().getStringArray(R.array.user_list));
        mListview.setAdapter(mAdapter);
        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                test(position + 1,context);
            }
        });

        userDatabaseHelper = new UserDatabaseHelper(context);
        return view;
    }

    //lisytView的功能，暂时只实现了注册功能
    private void test(int pos,Context context) {
        switch (pos) {
            case 1:
                testSignUp(context);//注册
                break;
            case 2:
                //testLogOut();//退出
                break;
            case 3:
                //testResetPasswrod();//重置密码
                break;
            case 4:
                //loginByPhoneCode();//通过手机验证码登陆
                break;
            case 5:
                //resetPasswordBySMS();//通过短信验证码来重置用户密码
                break;
            default:

        }
    }


    //1 注册，弹出用户信息注册界面
    private void testSignUp(final Context context) {
        //弹出用户注册页面
       LayoutInflater layoutInflater =(LayoutInflater)context.getSystemService
               (Context.LAYOUT_INFLATER_SERVICE);
        final View DialogView = layoutInflater.inflate(R.layout.login_dialog, null);

        new AlertDialog.Builder(context).setTitle("请输入注册信息").setView(DialogView).setPositiveButton("注册",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        myPhoneNumber = (EditText) DialogView.findViewById(R.id.dialog_et_number);
                        myPassword = (EditText) DialogView.findViewById(R.id.dialog_et_password);
                        String telephone = myPhoneNumber.getText().toString();
                        String password = myPassword.getText().toString();

                        //均不为空
                        if (!password.equals("") && !telephone.equals("")) {
                            //将获取的用户信息封装到MyUser类中
                            final MyUser user = new MyUser();
                            user.setTelephone(telephone);
                            user.setPassword(password);

                            Log.e("TAG", user.toString());

                            if(userDatabaseHelper.insertUserData(user,context)){
                                //跳转到新闻主界面
                                Intent intent = new Intent();
                                intent.setClass(context, MainActivity.class);
                                startActivity(intent);
                            }
                        }else {//如果任何一个为空
                            Toast.makeText(context,"请输入完整信息",Toast.LENGTH_SHORT).show();
                        }
                    }
                }).show();
    }
}
