package myandroid.mynews.Fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myandroid.mynews.NewsMessage;
import myandroid.mynews.PullListView;
import myandroid.mynews.R;
import myandroid.mynews.adapter.NewsAdapter;
import myandroid.mynews.utils.HttpUtils;

/**
 * Created by quxia on 2017/7/20.
 */

//下拉刷新新闻的界面
public class NewsFragment extends Fragment implements PullListView.RefreshListener{

    private String OUTCOME_MESSAGE = "我想看新闻";

    //listView 数据 适配器
    private PullListView mListView;
    private List<NewsMessage>  mNewsList = null;
   private NewsAdapter mNewsAdapter = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.news_list,container,false);

        //初始化listView
       mListView = (PullListView) view.findViewById(R.id.news_list);
      // mListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
       mListView.setInterface(this);

        //当mNewsList为null的时候，初始化数据
       if( mNewsList == null){
           mNewsList = new ArrayList<NewsMessage>();

           NewsMessage newsMessage = new NewsMessage(new Date(), NewsMessage.Type.OUTCOME,OUTCOME_MESSAGE);
           Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.ic_eighth);

           newsMessage.setIcon1(bitmap);
           newsMessage.setIcon2(bitmap);
           newsMessage.setIcon3(bitmap);

           newsMessage.setNewsArticle1("北京最强沙尘暴午后袭沪 当地叫停广场舞");
           newsMessage.setNewsArticle2("北京最强沙尘暴午后袭沪 当地叫停广场舞");
           newsMessage.setNewsArticle3("北京最强沙尘暴午后袭沪 当地叫停广场舞");

           newsMessage.setNewsSource1("网易新闻");
           newsMessage.setNewsSource2("网易新闻");
           newsMessage.setNewsSource3("网易新闻");

           newsMessage.setUrl1("http://news.163.com/15/0416/14/ANB2VKVC00011229.html");
           newsMessage.setUrl2("http://news.163.com/15/0416/14/ANB2VKVC00011229.html");
           newsMessage.setUrl3("http://news.163.com/15/0416/14/ANB2VKVC00011229.html");

           mNewsList.add(newsMessage);
       }

        //初始化适配器和更新画面
        mNewsAdapter = new NewsAdapter(getContext(),mNewsList);
        mListView.setAdapter(mNewsAdapter);
        mNewsAdapter.notifyDataSetChanged();
        mListView.setSelection(mNewsList.size()-1);
      //Log.e("TAG","newsMessage add");
     // Log.e("TAG", String.valueOf(mNewsList.size()));
        mListView.refreshComplete();
        return view;
    }


    //加载数据，刷新数据
    @Override
    public void onRefresh() {
        //Log.e("TAG","onRefresh");
        new Thread(){
            @Override
            public void run() {
                //通过接口获取NewsMessage对象
                NewsMessage fromMessage = HttpUtils.sendMessage(OUTCOME_MESSAGE,getContext());
                Message m = Message.obtain();
                m.obj = fromMessage;
                mHandler.sendMessage(m);
            };
        }.start();
        mListView.refreshComplete();
    }

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            NewsMessage fromMessage = (NewsMessage) msg.obj;

           if(fromMessage != null){
               //添加到mNewsList新闻列表中
               mNewsList.add(0,fromMessage);

               //通知适配器重新填充
               mNewsAdapter.notifyDataSetChanged();
               mListView.setSelection(mNewsList.size()-1);
           }
        }
    };
}
