package myandroid.mynews;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by quxia on 2017/7/20.
 */

@TargetApi(Build.VERSION_CODES.M)
public class PullListView extends ListView implements AbsListView.OnScrollListener{

    View header;  //下拉加载的画面
    int headerHeight;//画面的高度

    int firstVisibleItem;//firstVisibleItem==0表示第一个可见的item是listView的第一个元素
    int scrollState;   //滑动状态
    boolean isRemark; //是否正在下拉
    int startY;  //用户开始下拉的Y值

    int state; //下拉的状态
    final int NONE = 0; //正常状态
    final int PULL = 1; //显示下拉可以刷新状态
    final int RELEASE = 2; //提示松开可以刷新状态
    final int REFRESHING = 3; //正在刷新状态

    public PullListView(Context context) {
        super(context);
        initView(context);
    }

    public PullListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PullListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        //加载listView下拉显示画面
        LayoutInflater inflater = LayoutInflater.from(context);
        header = inflater.inflate(R.layout.news_header, null);

       //通知父布局所需要的宽和高
        measureView(header);

         //获取下拉加载的画面的高度
        headerHeight = header.getMeasuredHeight();
        Log.i("TAG", "headerHeight = " + headerHeight);

        //隐藏下拉画面
        topPadding(-headerHeight);

        //加载listView下拉显示画面
        this.addHeaderView(header);

        //滑动事件的监听
        this.setOnScrollListener(this);
    }

    //设置下拉画面的上边距
    private void topPadding(int topPadding) {
        header.setPadding(header.getPaddingLeft(),topPadding,
                header.getPaddingRight(),header.getPaddingBottom());
        header.invalidate();
    }

    private void measureView(View view) {
        ViewGroup.LayoutParams p = view.getLayoutParams();
        //如果加载的画面为空，新建，宽度MATCH_PARENT 高度WRAP_CONTENT
        if (p == null) {
            p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        //加载的画面的宽度
        int width = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        //加载的画面的高度
        int height;
        int tempHeight = p.height;
        if (tempHeight > 0) {//高度不是0
            height = MeasureSpec.makeMeasureSpec(tempHeight,
                    MeasureSpec.EXACTLY);
        } else {             //高度是0
            height = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        }
        view.measure(width, height);
    }

    //获取滑动状态
    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        scrollState = i;
    }

    //获取第一个可见的空间在listView的位置
    @Override
    public void onScroll(AbsListView absListView, int i, int i1, int i2) {
        firstVisibleItem = i;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            //下拉状态 判断是否可以刷新 获取开始下拉的Y值
            case MotionEvent.ACTION_DOWN:
                if (firstVisibleItem == 0) {
                    isRemark = true;
                    startY = (int) ev.getY();
                }
                break;

            case MotionEvent.ACTION_MOVE:
                onMove(ev);
                break;

            case MotionEvent.ACTION_UP:
                //如果在显示松开可以刷新的时候放手，则刷新数据
                if (state ==RELEASE) {
                    state = REFRESHING;
                    //刷新
                    refreshViewByState();
                    //外部调用实现数据的刷新
              refreshListener.onRefresh();
                }
                //如果在显示下拉可以刷新的时候放手，则不刷新数据，恢复正常状态
                else if (state == PULL) {
                    state = NONE;
                    isRemark = false;
                    //刷新
                    refreshViewByState();
                }
                break;
        }

        return super.onTouchEvent(ev);
    }



    private void onMove(MotionEvent ev) {
        //不在下拉可以刷新状态则返回
        if (!isRemark) {
            return;
        }
        int tempY = (int) ev.getY();

        //获取下拉的距离
        int space = tempY - startY;

        int topPadding = space - headerHeight;

        switch (state) {

            //正常状态
            case NONE:
                if (space > 0) {
                    state = PULL;
                    //刷新
                    refreshViewByState();
                }
                break;

            //显示下拉可以刷新状态
            case PULL:
                //逐渐显示画面
                topPadding(topPadding);

                if (space > headerHeight + 30
                        && scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    state =RELEASE;
                    //刷新
                    refreshViewByState();
                }
                break;

            //提示松开可以刷新状态
            case RELEASE:
                //逐渐显示画面
                topPadding(topPadding);

                if (space < headerHeight + 30) {
                    state = PULL;
                    //刷新
                    refreshViewByState();
                } else if (space <= 0) {
                    state = NONE;
                    isRemark = false;
                    //刷新
                    refreshViewByState();
                }
                break;
        }
    }

    //根据状态刷新页面，更改字体提示,箭头图片和进度条
    private void refreshViewByState() {

        //获取字体提示,箭头图片和进度条的id
        TextView tip = (TextView) header.findViewById(R.id.news_head_tip);
        ImageView arrow  = (ImageView)header.findViewById(R.id.news_head_arrow);
        ProgressBar progress = (ProgressBar) header.findViewById(R.id.news_head_progress);

        //设置释放动画
        RotateAnimation animRelease = new RotateAnimation(0, 180,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        animRelease.setDuration(500);
        animRelease.setFillAfter(true);

        //设置刷新动画
        RotateAnimation animRefreshing = new RotateAnimation(180, 0,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        animRefreshing.setDuration(500);
        animRefreshing.setFillAfter(true);

        //根据状态，更改字体提示,箭头图片和进度条
        switch (state) {
            case NONE:
                arrow.clearAnimation();
                topPadding(-headerHeight);
                break;

            case PULL:
                arrow.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                tip.setText("下拉可以刷新！");
                arrow.clearAnimation();
                arrow.setAnimation(animRefreshing);
                break;

            case RELEASE:
                arrow.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                tip.setText("松开可以刷新！");
                arrow.clearAnimation();
                arrow.setAnimation(animRelease);
                break;

            case REFRESHING:
                topPadding(50);
                arrow.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                tip.setText("正在刷新...");
                arrow.clearAnimation();
                break;
        }
    }

    //刷新完成后，设置下一次刷新的时间
    public void refreshComplete() {
        state = NONE;
        isRemark = false;
        refreshViewByState();
        TextView lastUpdateTime = (TextView) header
                .findViewById(R.id.news_head_date_time);
        //设置格式
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss");
        //取得当前时间
        Date date = new Date(System.currentTimeMillis());
        String time = format.format(date);
        //lastUpdateTime.setText(time);
       // refreshViewByState();
    }


    //设置接口类实现数据的刷新
    RefreshListener refreshListener;
    public void setInterface(RefreshListener refreshListener){
        this.refreshListener = refreshListener;
    }

    public interface RefreshListener{
        public void onRefresh();
    }
}

