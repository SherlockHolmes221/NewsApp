package myandroid.mynews;

/**
 * Created by quxia on 2017/7/21.
 */
public class CodeResult {

    //目标为筛选出新闻类
    //新闻类编码为302000
    //文字类编码为100000
    //文字加上url类编码为200000

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
