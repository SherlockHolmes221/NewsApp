package myandroid.mynews.Sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import myandroid.mynews.user.MyUser;

/**
 * Created by quxia on 2017/7/18.
 */


public class UserDatabaseHelper extends SQLiteOpenHelper {
    public UserDatabaseHelper(Context context) {


        super(context,"MyUserData", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
     sqLiteDatabase.execSQL("create table if not exists UserData" +
             "(_id integer primary key autoincrement," +
             "telephone varchar ," +
             "password varchar)");

    }

    public boolean insertUserData(MyUser user,Context context){
        SQLiteDatabase database = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("telephone",user.getTelephone());
        contentValues.put("password",user.getPassword());

        database.insert("UserData",null,contentValues);

        Toast.makeText(context,"注册成功",Toast.LENGTH_SHORT).show();
        Log.e("TAG",user.toString());
        return true;
    }



    public Cursor getAllUserData(){
        SQLiteDatabase database = getWritableDatabase();
        return database.query("UserData",null,null,null,null,null,"telephone "+"ASC");

    }

    public void deleteAllUserdata(){
        SQLiteDatabase database = getWritableDatabase();
        database.delete("UserData",null,null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
